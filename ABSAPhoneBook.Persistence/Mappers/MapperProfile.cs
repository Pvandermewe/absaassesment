﻿using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using ABSAPhoneBook.Persistence.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Persistence.Mappers
{
    public class MapperProfile: Profile
    {
        public MapperProfile()
        {
            CreateMap<PhoneBook, GetAllPhoneBookResponseAggregate>().ReverseMap();
            CreateMap<Entry, GetAllEntryResponseAggregate>().ReverseMap();
        }
    }
}
