﻿using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using ABSAPhoneBook.Domain.Extensions;
using ABSAPhoneBook.Domain.Interfaces;
using ABSAPhoneBook.Persistence.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ABSAPhoneBook.Persistence.Repositories
{
    public class PhoneBookRepository : IPhoneBookRepository
    {
        private readonly IMapper _mapper;
        private readonly IOptions<ConnectionStrings> _conStringsConfig;
        private readonly ABSA_PhonebookContext _context;

        public PhoneBookRepository(ABSA_PhonebookContext context, IOptions<ConnectionStrings> conStringsConfig, IMapper mapper)
        {
            _conStringsConfig = conStringsConfig;
            _context = context;
            _mapper = mapper;
        }

        public async Task<CaptureEntryResponseAggregate> CaptureEntryAsync(CaptureEntryAggregate entryAggregate, CancellationToken cancellationToken)
        {
            try
            {

                var entry = new Entry
                {
                    Name = entryAggregate.Name,
                    PhoneNumber = entryAggregate.PhoneNumber,
                    PhoneBookId = entryAggregate.PhoneBookId
                };

                await _context.Entries.AddAsync(entry, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
                var entryId = entry.PhoneBookId;

                return new CaptureEntryResponseAggregate()
                {
                    EntryId = entryId
                };
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }

        public async Task<CapturePhoneBookResponseAggregate> CapturePhoneBookAsync(CapturePhoneBookAggregate phoneBookAggregate, CancellationToken cancellationToken)
        {
            try
            {

                var phoneBook = new PhoneBook
                {
                    Name = phoneBookAggregate.Name
                };

                await _context.PhoneBooks.AddAsync(phoneBook, cancellationToken);
                await _context.SaveChangesAsync(cancellationToken);
                var phonebookId = phoneBook.PhoneBookId;

                return new CapturePhoneBookResponseAggregate()
                {
                    PhoneBookId = phonebookId
                };
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }

        public async Task<List<GetAllEntryResponseAggregate>> GetAllEntriesAsync(int phoneBookId, CancellationToken cancellationToken)
        {
            var entryList = await _context.Entries.Where(x => x.PhoneBookId == phoneBookId).ToListAsync();

            return _mapper.Map<List<GetAllEntryResponseAggregate>>(entryList);
        }

        public async Task<List<GetAllPhoneBookResponseAggregate>> GetAllPhoneBooksAsync(CancellationToken cancellationToken)
        {
            var phonebookList = await _context.PhoneBooks.ToListAsync();

            return _mapper.Map<List<GetAllPhoneBookResponseAggregate>>(phonebookList);
        }
    }
}
