﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates
{
    public class CapturePhoneBookResponseAggregate
    {
        public int PhoneBookId { get; set; }
    }
}
