﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates
{
    public class GetAllPhoneBookResponseAggregate
    {
        public int PhoneBookId { get; set; }
        public string Name { get; set; }
    }
}
