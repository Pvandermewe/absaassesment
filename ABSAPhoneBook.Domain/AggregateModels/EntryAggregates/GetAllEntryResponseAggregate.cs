﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Domain.AggregateModels.EntryAggregates
{
    public class GetAllEntryResponseAggregate
    {
        public int EntryId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneBookId { get; set; }
    }
}
