﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Domain.AggregateModels.EntryAggregates
{
    public class CaptureEntryResponseAggregate
    {
        public int EntryId { get; set; }
    }
}
