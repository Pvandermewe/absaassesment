﻿using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ABSAPhoneBook.Domain.Interfaces
{
    public interface IPhoneBookRepository
    {
        Task<CapturePhoneBookResponseAggregate> CapturePhoneBookAsync(CapturePhoneBookAggregate phoneBookAggregate, CancellationToken cancellationToken);
        Task<CaptureEntryResponseAggregate> CaptureEntryAsync(CaptureEntryAggregate entryAggregate, CancellationToken cancellationToken);
        Task<List<GetAllPhoneBookResponseAggregate>> GetAllPhoneBooksAsync(CancellationToken cancellationToken);
        Task<List<GetAllEntryResponseAggregate>> GetAllEntriesAsync(int phoneBookId, CancellationToken cancellationToken);
    }
}
