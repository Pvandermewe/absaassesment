﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Domain.Extensions
{
    public class ConnectionStrings
    {
        public string PhoneBookDbConnection { get; set; }
    }
}
