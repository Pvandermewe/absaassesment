﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSAPhoneBook.Application.Commands.CaptureEntry;
using ABSAPhoneBook.Application.Queries.GetAllEntries;
using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ABSAPhoneBookService.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntryController : ControllerBase
    {
        private IMediator Mediator { get; }
        public EntryController(IMediator mediator)
        {
            Mediator = mediator;
        }

        [HttpPost]
        [Route("AddEntry")]
        public async Task<IActionResult> AddEntry([FromBody] CaptureEntryAggregate request)
        {
            var model = new CaptureEntryCommand
            {
                TransferObject = request
            };

            var result = await Mediator.Send(model);

            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllEntriesByPhoneBook/{phoneBookId}")]
    public async Task<IActionResult> GetAllEntriesByPhoneBook(int phoneBookId)
        {
            var model = new GetAllEntriesQuery()
            {
                PhoneBookId = phoneBookId
            };

            var result = await Mediator.Send(model);

            return Ok(result);
        }
    }
}

