﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABSAPhoneBook.Application.Commands.CapturePhoneBook;
using ABSAPhoneBook.Application.Queries.GetAllPhoneBook;
using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ABSAPhoneBookService.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneBookController : ControllerBase
    {
        private IMediator Mediator { get; }
        public PhoneBookController(IMediator mediator)
        {
            Mediator = mediator;
        }

        [HttpPost]
        [Route("AddPhoneBook")]
        public async Task<IActionResult> AddPhoneBook([FromBody] CapturePhoneBookAggregate request)
        {
            var model = new CapturePhoneBookCommand
            {
                TransferObject = request
            };

            var result = await Mediator.Send(model);

            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllPhoneBooks")]
        public async Task<IActionResult> GetAllPhoneBooks()
        {
            var model = new GetAllPhoneBookQuery();

            var result = await Mediator.Send(model);

            return Ok(result);
        }
    }
}
