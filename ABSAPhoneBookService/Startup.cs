using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ABSAPhoneBook.Application.Commands.CapturePhoneBook;
using ABSAPhoneBook.Domain.Interfaces;
using ABSAPhoneBook.Persistence.Entities;
using ABSAPhoneBook.Persistence.Mappers;
using ABSAPhoneBook.Persistence.Repositories;
using MediatR;
using MediatR.Extensions.FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace ABSAPhoneBookService
{
    public class Startup
    {
        private IConfiguration _config;
        public Startup(IConfiguration config)
        {
            _config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
           

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "PhoneBook API", Version = "v1" });
            });

            services.AddCors();
            services.AddControllers();
            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            var domainAssembly = Assembly.GetExecutingAssembly();
            services.AddMediatR(domainAssembly);

            services.AddFluentValidation(new[] { domainAssembly });

            services.AddDbContext<ABSA_PhonebookContext>(
                options => options.UseSqlServer(_config.GetSection("ConnectionStrings").GetSection("PhoneBookDbConnection").Value)
                );

            services.AddTransient<IPhoneBookRepository, PhoneBookRepository>();
            services.AddMediatR(typeof(CapturePhoneBookCommandHandler).GetTypeInfo().Assembly);

            var mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperProfile());
            });
            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PhoneBook API");
            });

           

            
            app.UseRouting();

            app.UseCors(options =>
                        options.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
