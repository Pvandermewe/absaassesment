﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Queries.GetAllEntries
{
    public class GetAllEntriesQuery : IRequest<GetAllEntriesResponse>
    {
        public int PhoneBookId { get; set; }
    }
}
