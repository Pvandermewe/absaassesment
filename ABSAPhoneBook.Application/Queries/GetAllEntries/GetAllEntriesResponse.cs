﻿using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Queries.GetAllEntries
{
    public class GetAllEntriesResponse
    {
        public IEnumerable<GetAllEntryResponseAggregate> TransferObjectList { get; set; }
    }
}
