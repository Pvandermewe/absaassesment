﻿using ABSAPhoneBook.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ABSAPhoneBook.Application.Queries.GetAllEntries
{
    public class GetAllEntriesQueryHandler : IRequestHandler<GetAllEntriesQuery, GetAllEntriesResponse>
    {
        private readonly IPhoneBookRepository _phoneBookRepository;
        private readonly IMediator _mediator;

        public GetAllEntriesQueryHandler(
            IPhoneBookRepository phoneBookRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _phoneBookRepository = phoneBookRepository;
        }

        public async Task<GetAllEntriesResponse> Handle(GetAllEntriesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _phoneBookRepository.GetAllEntriesAsync(request.PhoneBookId, cancellationToken);

                var returnvalue = new GetAllEntriesResponse();
                returnvalue.TransferObjectList = entity;

                return returnvalue;
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }
    }
}
