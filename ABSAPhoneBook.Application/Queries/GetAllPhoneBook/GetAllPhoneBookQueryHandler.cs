﻿using ABSAPhoneBook.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ABSAPhoneBook.Application.Queries.GetAllPhoneBook
{
    public class GetAllPhoneBookQueryHandler : IRequestHandler<GetAllPhoneBookQuery, GetAllPhoneBookResponse>
    {
        private readonly IPhoneBookRepository _phoneBookRepository;
        private readonly IMediator _mediator;

        public GetAllPhoneBookQueryHandler(
            IPhoneBookRepository phoneBookRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _phoneBookRepository = phoneBookRepository;
        }

        public async Task<GetAllPhoneBookResponse> Handle(GetAllPhoneBookQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _phoneBookRepository.GetAllPhoneBooksAsync(cancellationToken);

                var returnvalue = new GetAllPhoneBookResponse();
                returnvalue.TransferObjectList = entity;

                return returnvalue;
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }
    }
}
