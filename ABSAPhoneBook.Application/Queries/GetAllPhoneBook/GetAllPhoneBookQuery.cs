﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Queries.GetAllPhoneBook
{
    public class GetAllPhoneBookQuery : IRequest<GetAllPhoneBookResponse>
    {
    }
}
