﻿using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Queries.GetAllPhoneBook
{
    public class GetAllPhoneBookResponse
    {
        public IEnumerable<GetAllPhoneBookResponseAggregate> TransferObjectList { get; set; }
    }
}
