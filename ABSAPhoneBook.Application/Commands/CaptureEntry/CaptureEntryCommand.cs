﻿using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Commands.CaptureEntry
{
    public class CaptureEntryCommand : IRequest<CaptureEntryResponse>
    {
        public CaptureEntryAggregate TransferObject { get; set; }
    }
}

