﻿using ABSAPhoneBook.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ABSAPhoneBook.Application.Commands.CaptureEntry
{
    public class CaptureEntryCommandHandler : IRequestHandler<CaptureEntryCommand, CaptureEntryResponse>
    {
        private readonly IPhoneBookRepository _phoneBookRepository;
        private readonly IMediator _mediator;

        public CaptureEntryCommandHandler(
            IPhoneBookRepository phoneBookRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _phoneBookRepository = phoneBookRepository;
        }

        public async Task<CaptureEntryResponse> Handle(CaptureEntryCommand request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            try
            {
                var response = await _phoneBookRepository.CaptureEntryAsync(request.TransferObject, cancellationToken);

                return new CaptureEntryResponse()
                {
                    Response = response
                };

            }
            catch (Exception e)
            {
                //Add to serilog and it'll push it into App insight.
                Console.WriteLine(e.InnerException);
                throw;
            }
        }
    }
}

