﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Commands.CaptureEntry
{
    public class CaptureEntryValidator : AbstractValidator<CaptureEntryCommand>
    {
        public CaptureEntryValidator()
        {
            RuleFor(x => x.TransferObject.Name).NotEmpty();
            RuleFor(x => x.TransferObject.PhoneNumber).NotEmpty();
            RuleFor(x => x.TransferObject.PhoneNumber).NotNull();
        }
    }
}