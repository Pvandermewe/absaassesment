﻿using ABSAPhoneBook.Domain.AggregateModels.EntryAggregates;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Commands.CaptureEntry
{
    public class CaptureEntryResponse
    {
        public CaptureEntryResponseAggregate Response { get; set; }
    }
}
