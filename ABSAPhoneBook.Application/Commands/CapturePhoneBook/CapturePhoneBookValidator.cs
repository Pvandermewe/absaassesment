﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Commands.CapturePhoneBook
{
    public class CapturePhoneBookValidator : AbstractValidator<CapturePhoneBookCommand>
    {
        public CapturePhoneBookValidator()
        {
            RuleFor(x => x.TransferObject.Name).NotEmpty();
        }
    }
}