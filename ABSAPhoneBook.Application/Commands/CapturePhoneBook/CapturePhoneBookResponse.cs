﻿using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Commands.CapturePhoneBook
{
    public class CapturePhoneBookResponse
    {
        public CapturePhoneBookResponseAggregate Response { get; set; }
    }
}
