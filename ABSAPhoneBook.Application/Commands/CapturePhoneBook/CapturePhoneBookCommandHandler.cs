﻿using ABSAPhoneBook.Domain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ABSAPhoneBook.Application.Commands.CapturePhoneBook
{
    public class CapturePhoneBookCommandHandler : IRequestHandler<CapturePhoneBookCommand, CapturePhoneBookResponse>
    {
        private readonly IPhoneBookRepository _phoneBookRepository;
        private readonly IMediator _mediator;

        public CapturePhoneBookCommandHandler(
            IPhoneBookRepository phoneBookRepository,
            IMediator mediator)
        {
            _mediator = mediator;
            _phoneBookRepository = phoneBookRepository;
        }

        public async Task<CapturePhoneBookResponse> Handle(CapturePhoneBookCommand request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            try
            {
                var response = await _phoneBookRepository.CapturePhoneBookAsync(request.TransferObject, cancellationToken);

                return new CapturePhoneBookResponse()
                {
                    Response = response
                };

            }
            catch (Exception e)
            {
                //Add to serilog and it'll push it into App insight.
                Console.WriteLine(e.InnerException);
                throw;
            }
        }
    }
}

