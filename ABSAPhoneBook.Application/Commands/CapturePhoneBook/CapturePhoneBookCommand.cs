﻿using ABSAPhoneBook.Domain.AggregateModels.PhoneBookAggregates;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABSAPhoneBook.Application.Commands.CapturePhoneBook
{
    public class CapturePhoneBookCommand : IRequest<CapturePhoneBookResponse>
    {
        public CapturePhoneBookAggregate TransferObject { get; set; }
    }
}

