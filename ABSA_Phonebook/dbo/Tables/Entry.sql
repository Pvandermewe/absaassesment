﻿CREATE TABLE [dbo].[Entry]
(
	[EntryId] INT NOT NULL PRIMARY KEY IDENTITY (1,1), 
    [Name] NVARCHAR(250) NULL, 
    [PhoneNumber] NVARCHAR(250) NULL, 
    [PhoneBookId] INT NOT NULL, 
    CONSTRAINT [FK_Entry_PhoneBook] FOREIGN KEY ([PhoneBookId]) REFERENCES [PhoneBook]([PhoneBookId])
)
